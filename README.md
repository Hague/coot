# coot

Rough and ready clipboard manager for wayland using bemenu, wl-copy, and wl-paste.

Use

    ruby coot.rb select

to select from copy history. The result will be put into both system and
primary clipboards.

Use

    ruby coot.rb clear

to clear all history and clipboards (removes e.g. passwords).

To get history you can try

    ruby coot.rb daemon

but this polls wl-paste every second. wl-paste on the primary clipboard
causes weirdness with X applications.

Instead i'll just use

    ruby coot.rb once

to copy the current clipboards into history.

Better yet

    ruby coot.rb once select

to copy current clipboards into history, then make a selection. This can
be used to sync both clipboards.

# requires

* wl-paste/wl-copy (probably wl-clipboard package)
* bemenu
