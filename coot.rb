#!/usr/bin/ruby

require 'json'
require 'open3'

$HIST_FILE = File.join(ENV['HOME'], '/.cache/coot.json')

$MONITOR = 'monitor'
$SELECT = 'select'
$CLEAR = 'clear'
$ONCE = 'once'
$ONCE_SYSTEM = 'once_system'
$ONCE_PRIMARY = 'once_primary'

$GET_SYSTEM = 'wl-paste 2> /dev/null'
$GET_PRIMARY = 'wl-paste --primary 2> /dev/null'

$SET_SYSTEM = 'wl-copy'
$SET_PRIMARY = 'wl-copy --primary'

$ONCE_CMD = "/usr/bin/ruby #{File.expand_path(__FILE__)} %s"
$WATCH_SYSTEM = "wl-paste --watch #{$ONCE_CMD % $ONCE_SYSTEM} 2> /dev/null"
$WATCH_PRIMARY = "wl-paste --primary --watch #{$ONCE_CMD % $ONCE_PRIMARY} 2> /dev/null"

$CLEAR_SYSTEM = 'wl-copy --clear'
$CLEAR_PRIMARY = 'wl-copy --primary --clear'

$MENU_CMD = "bemenu --prompt 'Choose a paste' --ignorecase"
$HIST_LEN = 10

def read_history()
    begin
        return JSON.parse(File.read($HIST_FILE))
    rescue Errno::ENOENT
        return []
    end
end

def write_history(history)
    File.open($HIST_FILE, 'w+') { |f| f.write(history.to_json) }
end

def clear_history()
    if File.exist?($HIST_FILE)
        File.delete($HIST_FILE)
    end
    `#{$CLEAR_SYSTEM}`
    `#{$CLEAR_PRIMARY}`
end

def print_usage()
    puts "Usage: coot.rb (#{$MONITOR}|#{$SELECT}|#{$CLEAR})*"
    puts 'where'
    puts "  #{$MONITOR} -- start the monitoring for changes to clipboards (can interfere with X programs)"
    puts "  #{$ONCE} -- copy both clipboards to hist"
    puts "  #{$ONCE_SYSTEM} -- copy system clipboard to hist"
    puts "  #{$ONCE_PRIMARY} -- copy primary clipboard to hist"
    puts "  #{$SELECT} -- copy item from history (uses bemenu)"
    puts "  #{$CLEAR} -- clear history"
end

# Adds an entry to the history if it is not empty
# Params:
# +get_cmd+ - the system command used to get the contents
# +history+ - array of history to add new items to
# +last_read+ - the last value read with get_cmd (only add if new)
# Returns:
# the entry read with get_cmd
def add_entry(get_cmd, history, last_read = "")
    entry = `#{get_cmd}`.chomp
    if entry != "" and entry != last_read
        history.delete entry
        history.unshift entry
        if history.length > $HIST_LEN
            history.pop
        end
    end
    return entry
end

def run_monitor()
    history = read_history()

    system_thread = Process.detach(Process.spawn($WATCH_SYSTEM))
    primary_thread = Process.detach(Process.spawn($WATCH_PRIMARY))

    system_thread.join
    primary_thread.join
end

def run_once()
    history = read_history()
    add_entry($GET_SYSTEM, history)
    add_entry($GET_PRIMARY, history)
    write_history(history)
end

def run_once_cmd(get_command)
    history = read_history()
    add_entry(get_command, history)
    write_history(history)
end

def menu_history(history)
    # add index, remove newlines in entries, put newlines between
    return history.each_with_index
                  .map {|s,i| "#{i}: #{s.gsub("\n", '')}"}
                  .join("\n")
end

# Parse result of menu command
# Params:
# +result+ string returned by menu
# Returns:
# index of history to copy or -1 if none
def get_menu_index(result)
    if result != nil and result != ""
        return result[0].to_i
    else
        return -1
    end
end

def select()
    history = read_history()

    result = ""
    Open3.popen3($MENU_CMD) { |menu_in,menu_out|
        menu_in.write(menu_history(history))
        menu_in.close
        result = menu_out.gets
    }

    index = get_menu_index(result)

    if index >= 0
        new_clip = history[get_menu_index(result)]

        Open3.popen3($SET_SYSTEM) { |clip_in|
            clip_in.write(new_clip)
        }
        Open3.popen3($SET_PRIMARY) { |clip_in|
            clip_in.write(new_clip)
        }
    end
end

def main()
    ARGV.each { |cmd|
        case cmd
        when $MONITOR
            run_monitor()
        when $ONCE
            run_once()
        when $ONCE_SYSTEM
            run_once_cmd($GET_SYSTEM)
        when $ONCE_PRIMARY
            run_once_cmd($GET_PRIMARY)
        when $SELECT
            select()
        when $CLEAR
            clear_history()
        else
            print_usage()
        end
    }
end

main()
